---
output: pdf_document
papersize: a4
geometry: "left=2cm, right=1.5cm, top=3cm, bottom=1.5cm"
header-includes:
  - \usepackage{booktabs}
  - \usepackage{pdflscape}
  - \usepackage{float}
  - \usepackage{fancyhdr}
  - \pagestyle{fancy}
  - \fancyfoot[L]{Saidou MAHMOUDOU}
  - \fancyfoot[C]{}
  - \renewcommand{\headrulewidth}{0pt}
  - \fancyfoot[LE,RO]{\thepage}
  - \renewcommand{\familydefault}{\sfdefault}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-12.5, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\begin{center}
\Large{\textbf{Comparaison de la performance des tests POC CCA, UCAA500 et la filtration Urinaire dans le cadre de 2 études : Freebily et BenziR}}
\end{center}

```{r setup, include=FALSE}
options(knitr.kable.NA = "-")
library(dplyr)
library(knitr)
library(kableExtra)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_1 %>%
  kable(caption = "Nombre des Participants",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N"), align = c("l", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_2 %>%
  kable(caption = "Caractéristiques des Participants",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "Total, n(%)", "Enfants en âge scolaire, n(%)", "Femmes enceintes, n(%)"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Age", 1,  6) %>%
  pack_rows("Sexe", 7, nrow(tab_2))
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_3 %>%
  kable(caption = "Résultats diagnostic de la schistosomiase urogénital",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "Total, n(%)", "Enfants en âge scolaire, n(%)", "Femmes enceintes, n(%)"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("POC CCA", 1, 2) %>%
  pack_rows("Microscopie", 3, 4) %>%
  pack_rows("UCAA500", 5, 7) %>%
  pack_rows("UCAA500 + microscopie", 8, nrow(tab_3))
```

\begin{landscape}

```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_4 %>%
  kable(caption = "Comparaison des caractéristiques de performance de POC CCA et l’UCAA500 par rapport à la microscopie chez les enfants en âge scolaire et les femmes enceintes",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "POC CCA, (95% IC)", "UCAA500, (95% IC)", "POC CCA, (95% IC)", "UCAA500, (95% IC)",
                      "POC CCA, (95% IC)", "UCAA500, (95% IC)"),
        align = c("l", "r", "r", "r", "r", "r", "r"), format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  add_header_above(c(" " = 1, "Total" = 2, "Enfants en âge scolaire" = 2, "Femmes enceintes" = 2), bold = TRUE) %>%
  row_spec(0, bold = TRUE)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
tab_5 %>%
  kable(caption = "Comparaison des caractéristiques de performance de POC CAA par rapport à UCAA500 et la composite (microscopie + UCAA500) chez les enfants en âge scolaire et les femmes enceintes",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "UCAA500, (95% IC)", "Composite, (95% IC)", "UCAA500, (95% IC)", "Composite, (95% IC)",
                      "UCAA500, (95% IC)", "Composite, (95% IC)"),
        align = c("l", "r", "r", "r", "r", "r", "r"), format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  add_header_above(c(" " = 1, "Total" = 2, "Enfants en âge scolaire" = 2, "Femmes enceintes" = 2), bold = TRUE) %>%
  row_spec(0, bold = TRUE)
```

\end{landscape}